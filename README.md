## Instructions

### Install

#### 1) Install Docker & Docker Compose

Get Docker-ce from here https://docs.docker.com/install/ 

https://docs.docker.com/compose/install/

#### 2) Clone this repository

#### 3) Clone MediaWiki Core & the Vector Skin (default skin)

From [Wikimedia Gerrit](https://gerrit.wikimedia.org/r/#/admin/projects/mediawiki/core):

```
git clone https://gerrit.wikimedia.org/r/mediawiki/core ~/dev/src/mediawiki
git clone https://gerrit.wikimedia.org/r/mediawiki/skins/Vector ~/dev/src/mediawiki/skins/Vector
```

#### 4) Run `composer install` for MediaWiki

In the mediawiki repo directory run composer install:
either use the docker one:
```
docker run -it --rm -v ~/.composer:/tmp -v $(pwd):/app docker.io/composer install
```
or one you have installed:
```
composer install
```

#### 5) Create a basic LocalSettings.php

The .docker/LocalSettings.php file will exist within the containers running Mediawiki.

Make a LocalSettings.php in the root of the Mediawiki repo containing the following:

```
<?php
require_once __DIR__ . '/.docker/LocalSettings.php';
wfLoadSkin( 'Vector' );
```

### 6) Configure the environment

Copy the contents of `default.env` from the `mediawiki-docker-dev` dir into a new file called `local.env`.

Alter any settings that you want to change, for example the install location of MediaWiki, a directory to a local composer cache, webserver or php version. Note nginx may not work.

#### 7) Launch the environment

**Create and start the Docker containers:**

```
./create
```

**Update your hosts file:**

Add the following to your `/etc/hosts` file:

```
127.0.0.1 default.web.mw.localhost # mediawiki-docker-dev
```

## Commands

You can setup a bash alias to make running the commands easier.

```bash
alias mw-docker-dev='_(){ (cd /$GITPATH/github/addshore/mediawiki-docker-dev; ./$@) ;}; _'
```


### Create

Create and start containers.

This includes setting up a default wiki @ http://default.web.mw.localhost:8080 with an "admin" user that has password "adminpass".

You can choose the spec of the system that the create command will set up by using a custom .env file called local.env and customizing the variables.

```
mw-docker-dev create 
```

### Stop

Shuts down the containers. Databases and other volumes persist.

```
mw-docker-dev stop
```

### Start

Start (or restart) the containers, if things have already been created using `./create`.

```
mw-docker-dev start
```

### Destroy

Stop and delete the containers. Also removes databases and volumes.

```
mw-docker-dev destroy
```

### Bash

Run commands on the webserver.

If the containers are running you can use `./bash` to open up an interactive shell on the webserver.

This can be used to run PHPUnit tests, maintenance scripts, etc.

```
mw-docker-dev bash
```

### Add site

You can add a new site by subdomain name using the ./addsite command
Add the name to the hosts file manually
```
mw-docker-dev addsite enwiki
```

### PHPUnit

```
mw-docker-dev phpunit-file default extensions/FileImporter/tests/phpunit
```

### QUnit

To run the QUnit tests from the browser, use [Special:JavaScriptTest](http://default.web.mw.localhost:8080/index.php?title=Special:JavaScriptTest).

See also <https://www.mediawiki.org/wiki/Manual:JavaScript_unit_testing>.

To run QUnit from the command-line, make sure you have [Node.js v4 or later](https://nodejs.org/) installed on the host, and set the following environment variables:

```
export MW_SERVER='http://default.web.mw.localhost:8080'
export MW_SCRIPT_PATH='/mediawiki'
```

```
$ cd ~/dev/mediawiki
$ npm install
$ npm run qunit
```

## Access

 - [Default MediaWiki Site](http://default.web.mw.localhost:8080)

## Debugging

While using PHP you can use remote xdebug debugging.

To do so you need to set `IDELOCALHOST` in you local.env file to the IP of your local machine (where you run your IDE) as it appears to docker. Note with Docker for Mac, you can use `IDELOCALHOST=host.docker.internal`.

xdebug connections will then be sent to this IP address on port 9000.
